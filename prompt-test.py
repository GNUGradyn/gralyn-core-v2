from prompt_toolkit import PromptSession
from prompt_toolkit.patch_stdout import patch_stdout
from prompt_toolkit import print_formatted_text
from prompt_toolkit.formatted_text import FormattedText
import asyncio

async def printer():
    while True:
        print_formatted_text(FormattedText([('#0000FF', '[I] '),('', "DATA TIME")]))
        await asyncio.sleep(1)

async def prompter():
    session = PromptSession()
    while True:
        with patch_stdout():
            input = await session.prompt_async('$ ')

async def main():
    printer_task = asyncio.create_task(printer())
    await prompter()
    await printer_task

asyncio.run(main())
