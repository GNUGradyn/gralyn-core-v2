from contextlib import suppress
from prompt_toolkit import PromptSession
from prompt_toolkit.patch_stdout import patch_stdout
from prompt_toolkit import print_formatted_text
from prompt_toolkit.formatted_text import FormattedText
from discord.ext import commands
import discord
import configparser
import asyncio
from logging import Logger
import inspect 
import sys
import traceback
import os
import sqlite3

#regioon config
def save_config(*args): #ignore any arguments in case self is passed 
    with open("config.ini", "w") as configFile:
        config.write(configFile)
configparser.ConfigParser.save = save_config
config = configparser.ConfigParser()
config.read('config.ini')
#endregion
#region console
class log:
    """
    Log info to the console/to file
    log.info(): Used for regular operation
    log.warn(): Used for abnormal events that do not prevent the bot from operating
    log.err(): Used for abnormal events that prevent the bot from operating
    """
    def info(out):
        """Used for regular operation"""
        print_formatted_text(FormattedText([('#0000FF', '[I]'), ('', get_file() + " "),('', out)]))
    def warn(out):
        """Used for abnormal events that do not prevent the bot from operating"""
        print_formatted_text(FormattedText([('#FFFF00', '[!]'), ('', get_file() + " "),('', out)]))
    def err(out):
        """Used for abnormal events that prevent the bot from operating"""
        print_formatted_text(FormattedText([('#FF0000', '[E]'), ('', get_file() + " "),('', out)]))
def get_file():
    file = ""
    if not Logger.findCaller(None)[0] == "main.py": 
        filename = Logger.findCaller(None)[0].split('\\')[-1].split('/')[-1][:-3] #TODO: possibly a better way to get the file name from the path?
        file = f"[plugins.{filename}]" 
    else:
        file = "[core]"
    return file
console_commands = []
def console_command(command):
    """Decorator for console commands"""
    console_commands.append(command)
async def console():
    prompt_data = {
        "guild": {
            "name": "",
            "id": ""
        },
        "channel": {
            "name": "",
            "id": "" 
        },
        "echo": None 
    }
    session = PromptSession()
    while True:
        with patch_stdout():
            input = await session.prompt_async('$ ')
            try:
                command = input.split(" ")[0]
                args = input.split(" ")[1:]
                for command_func in console_commands:
                    if command_func.__name__ == command:
                        if inspect.iscoroutinefunction(command_func):
                            await command_func(args)
                        else:   
                            command_func(args)
            except Exception as e:
                log.err(f"An error occured running command {command}\n{traceback.format_exc()}")
#endregion

async def discord_bot():
    def prefix(bot, message): # Plugins can overwrite this for improved prefix handling
        return config["Core"]["prefix"]
    global bot
    bot = commands.AutoShardedBot(prefix, intents=discord.Intents.all()) #TODO: Sharding
    bot.log = log
    bot.config = config
    log.info("Loading core")
    bot.add_cog(Core(bot))
    for plugin in os.listdir("plugins"):
        if os.path.isdir("plugins/" + plugin):
            continue
        plugin = "plugins." + plugin[:-3]
        log.info(f"Loading {plugin}")
        bot.load_extension(plugin)
    log.info("Connecting to discord")
    await bot.start(config["Core"]["token"])
class DatabaseDriver:
    """Abstract Base Class for database drivers. Make sure to sanatize any database inputs!"""
    def get_user(self, id):
        raise NotImplementedError
    def get_guild(self, id):
        raise NotImplementedError
    def push_user(self, id, update):
        raise NotImplementedError
    def push_guild(self, id, update):
        raise NotImplementedError
    def disconnect(self):
        raise NotImplementedError
class DefaultDatabaseDriver(DatabaseDriver):
    """Default database driver used by the bot. Supports sqlite and postgresql"""
    def __init__(self):
        if config["Core-Advanced"]["database"] == "sqlite":
            log.info("Connecting to sqlite database")
            self.con = sqlite3.connect('gralyn.db')
            self.cur = self.con.cursor()
    def get_user(self, id):
        if config["Core-Advanced"]["database"] == "sqlite":
            results = self.cur.execute("SELECT * FROM users WHERE id=?", (id)).fetchall()
            if len(results) == 0:
                return None
            if len(results) == 1:
                return results[0]
            if len(results) > 1:
                log.warn(f"Multiple results found for user {id} in database! Returning all matches, This is probably a bug")
                return results
    def get_guild(self, id):
        pass
    def push_user(self, id, user):
        pass
    def push_guild(self, id, guild):
        pass
    def disconnect(self):
        self.con.close()

class Core(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
    @commands.Cog.listener()
    async def on_ready(self):
        log.info("Connected to Discord")
    @console_command
    def ping(args):
        log.info("Pong!")
    @console_command
    async def stop(args):
        if len(args) > 0:
            if args[0] == "now":
                log.warn("Force stopping Gralyn Core")
                sys.exit(1)
        log.warn("Stopping Gralyn Core")
        await bot.close()
        log.info("Disconnected from Discord")
        console_task.cancel()
        log.info("Stopped console")
        database.disconnect()
        log.info("Disconnected from database")
    @console_command
    def prefix(args):
        print(config["Core"]["prefix"]) #TODO: Changing prefix from here
    @console_command
    def reload(args):
        if args == 0:
            pass #TODO: Reload all
        else:
            plugin = " ".join(args)
            if plugin[:8] != "plugins.":
                plugin = "plugins." + plugin
            log.info(f"Reloading {plugin}")
            bot.reload_extension(plugin)
    @console_command
    def load(args):
        if args == 0:
            log.err("Please specify a plugin to load")
            return
        plugin = " ".join(args)
        if plugin[:8] != "plugins.":
            plugin = "plugins." + plugin
        log.info(f"Loading {plugin}")
        bot.load_extension(plugin)
    @console_command
    def unload(args):
        if args == 0:
            log.err("Please specify a plugin to unload")
            return
        plugin = " ".join(args)
        if plugin[:8] != "plugins.":
            plugin = "plugins." + plugin
        log.info(f"Unloading {plugin}")
        bot.unload_extension(plugin)

async def main():
    global console_task
    console_task = asyncio.create_task(console())
    global database
    database = DefaultDatabaseDriver() # TODO: Allow this to be changed by plugins
    await discord_bot()
    await console_task

with suppress(asyncio.CancelledError): # Suppress CancelledError to avoid an error when killing the console on exit
#TODO: Only suppress cancelledError if we are cancelling the console and gracefully exiting
    asyncio.run(main())
#TODO: auto generate config.ini